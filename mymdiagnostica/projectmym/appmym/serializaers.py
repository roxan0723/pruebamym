from asyncore import read
from django.forms import CharField
from rest_framework import serializers
from.models import *


class SolicitudSerializers(serializers.ModelSerializer):
    class Meta: 
     model = Solicitud
     fields = all


class LegalizacionSerializers(serializers.ModelSerializer):
    solicitud_id=SolicitudSerializers(many=False, read_only=True)
    class Meta: 
     model = Legalizacion
     fields = all


class DetalleSerializers(serializers.ModelSerializer):
    legalizacion_id = LegalizacionSerializers (many=False, read_only= True)
    class Meta: 
     model = Detalle
     fields = all
