from django.urls import path, include
from  . import views


urlpatterns = [
    # path('', views.inicio),
    path('', views.solicitud),
    path('legalizacion/', views.legalizacion, name='legalizacion_form'),
    path('detalle/', views.detalle, name='detalle_form'),

]