from django.apps import AppConfig


class AppmymConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'appmym'
