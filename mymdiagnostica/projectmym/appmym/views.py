from django.http import HttpResponse
from django.shortcuts import render,redirect
from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework import status
from datetime import date
from rest_framework.response import Response


from projectmym.appmym.models import Detalle, Legalizacion, Solicitud
from projectmym.appmym.serializaers import DetalleSerializers, LegalizacionSerializers, SolicitudSerializers


# Create your views here.

def solicitud (request):
    return render (request,'paginas/solicitud.html')
    
def legalizacion (request):
    return render (request,'paginas/legalizacion.html')

def detalle (request):
    return render (request,'paginas/detalle.html')

#Solicitud
@api_view(['GET', 'POST'])
def get_solicitud(request):

    if request.method == 'GET':
        funciones_solicitud = Solicitud.objects.all() 
        serializer = SolicitudSerializers(funciones_solicitud, many=True)
        return Response(serializer.data)


    elif request.method == 'POST':
        serializer = SolicitudSerializers(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status= status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def putdelete_solicitud(request, pk):
    
    try:
        funciones_solicitud = Solicitud.objects.get(pk=pk)
    except Solicitud.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = SolicitudSerializers(funciones_solicitud)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = SolicitudSerializers(funciones_solicitud, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        funciones_solicitud.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

#Legalizacion
@api_view(['GET', 'POST'])
def get_solicitud(request):

    if request.method == 'GET':
        funciones_legalizacion = Legalizacion.objects.all() 
        serializer = LegalizacionSerializers( funciones_legalizacion, many=True)
        return Response(serializer.data)


    elif request.method == 'POST':
        serializer = LegalizacionSerializers(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status= status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def putdelete_legalizacio(request, pk):
    
    try:
        funciones_legalizacion = Legalizacion.objects.get(pk=pk)
    except Solicitud.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = LegalizacionSerializers( funciones_legalizacion)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = LegalizacionSerializers( funciones_legalizacion, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        funciones_legalizacion.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


#detalle
@api_view(['GET', 'POST'])
def get_detalle(request):

    if request.method == 'GET':
        funciones_detalle = Detalle.objects.all() 
        serializer = DetalleSerializers(funciones_detalle, many=True)
        return Response(serializer.data)


    elif request.method == 'POST':
        serializer = DetalleSerializers(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status= status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def putdelete_solicitud(request, pk):
    
    try:
        funciones_detalle = Detalle.objects.get(pk=pk)
    except Solicitud.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = DetalleSerializers(funciones_detalle)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = DetalleSerializers(funciones_detalle, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        funciones_detalle.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)