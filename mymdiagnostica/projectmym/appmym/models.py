from tokenize import blank_re
from django.db import models

# Create your models here.

class Solicitud (models.Model):
    id_solicitud = models.AutoField (primary_key = True)
    fecha = models.DateField (blank=False, auto_now_add=True)
    nombre_completo = models.CharField (max_length=100,blank=False)
    cargo = models.CharField (max_length=100,blank=False)
    lugar_desplazamiento = models.TextField (blank=False)
    monto = models.IntegerField (blank=False)
    fecha_viaje = models.DateField (blank=False, auto_now_add=True)

    class Meta:
        verbose_name= 'Solicitud'
        verbose_name_plural= 'Solicitudes'



class Legalizacion (models.Model):
    id_legalizacion = models.AutoField (primary_key = True)
    solicitud_id = models.ForeignKey(Solicitud, on_delete=models.CASCADE)
    fecha_final = models.DateField (blank=False)
    numero_dias = models.CharField (max_length=100,blank=False)
    monto_asignado = models.IntegerField (blank=False)
    valor_gastado = models.IntegerField (blank=False)
    diferencia = models.IntegerField (blank=False)
    fecha_legalizacion = models.DateTimeField (blank=False)


    class Meta:
        verbose_name= 'Legalizacion'
        verbose_name_plural= 'Legalizaciones'

    

class Detalle (models.Model):
    id_detalle = models.AutoField (primary_key = True)
    legalizacion_id = models.ForeignKey(Legalizacion, on_delete=models.CASCADE)
    fecha = models.DateField (blank=False)
    nit_proveedor = models.CharField (max_length=100,blank=False)
    nombre_proveedor = models.CharField (max_length=100,blank=False)
    concepto = models.TextField (blank=False)
    valor = models.IntegerField (blank=False)
    detalle = models.TextField (blank=False)


    class Meta:
        verbose_name= 'Detalle'
        verbose_name_plural= 'Detalles'



