# Generated by Django 4.1.2 on 2022-10-30 19:10

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Solicitud',
            fields=[
                ('id_solicitud', models.AutoField(primary_key=True, serialize=False)),
                ('fecha', models.DateField(auto_now_add=True)),
                ('nombre_completo', models.CharField(max_length=100)),
                ('cargo', models.CharField(max_length=100)),
                ('lugar_desplazamiento', models.TextField()),
                ('monto', models.IntegerField()),
                ('fecha_viaje', models.DateField(auto_now_add=True)),
            ],
            options={
                'verbose_name': 'Solicitud',
                'verbose_name_plural': 'Solicitudes',
            },
        ),
        migrations.CreateModel(
            name='Legalizacion',
            fields=[
                ('id_legalizacion', models.AutoField(primary_key=True, serialize=False)),
                ('fecha_final', models.DateField()),
                ('numero_dias', models.CharField(max_length=100)),
                ('monto_asignado', models.IntegerField()),
                ('valor_gastado', models.IntegerField()),
                ('diferencia', models.IntegerField()),
                ('fecha_legalizacion', models.DateTimeField()),
                ('solicitud_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='appmym.solicitud')),
            ],
            options={
                'verbose_name': 'Legalizacion',
                'verbose_name_plural': 'Legalizaciones',
            },
        ),
        migrations.CreateModel(
            name='Detalle',
            fields=[
                ('id_detalle', models.AutoField(primary_key=True, serialize=False)),
                ('fecha', models.DateField()),
                ('nit_proveedor', models.CharField(max_length=100)),
                ('nombre_proveedor', models.CharField(max_length=100)),
                ('concepto', models.TextField()),
                ('valor', models.IntegerField()),
                ('detalle', models.TextField()),
                ('legalizacion_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='appmym.legalizacion')),
            ],
            options={
                'verbose_name': 'Detalle',
                'verbose_name_plural': 'Detalles',
            },
        ),
    ]
